import unittest
from unittest import TestCase


def fizzbuzz(my_integer: int) -> str:
    if my_integer % 3 == 0 and my_integer % 5 == 0:
        return 'fizzbuzz'
    elif my_integer % 3 == 0:
        return 'fizz'
    elif my_integer % 5 == 0:
        return 'buzz'

    return f'{my_integer}'


class TestFizzBuzz(TestCase):

    def test_1_display_1(self):
        # given
        my_integer = 1
        # when
        result = fizzbuzz(my_integer)
        # then
        self.assertEqual(result, '1')

    def test_2_display_2(self):
        # given
        my_integer = 2
        # when
        result = fizzbuzz(my_integer)
        # then
        self.assertEqual(result, '2')

    def test_3_display_fizz(self):
        # given
        my_integer = 3
        # when
        result = fizzbuzz(my_integer)
        # then
        self.assertEqual(result, 'fizz')

    def test_5_display_buzz(self):
        # given
        my_integer = 5
        # when
        result = fizzbuzz(my_integer)
        # then
        self.assertEqual(result, 'buzz')

    def test_15_display_fizzbuzz(self):
        # given
        my_integer = 15
        # when
        result = fizzbuzz(my_integer)
        # then
        self.assertEqual(result, 'fizzbuzz')


if __name__ == '__main__':
    unittest.main()
